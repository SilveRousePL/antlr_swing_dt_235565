tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
  Integer numer            = 0;
  Integer condition_number = 0;
  Integer while_number = 0;
}

prog        : (e+=scopeb | e+=expr | d+=declaration)* -> program(name={$e},declarations={$d});
scopeb      : ^(LC {enterScope();} (e+=scopeb | e+=expr | d+=declaration)* {leaveScope();}) -> scopeBlock(wyr={$e},decl={$d});
declaration : ^(VAR i1=ID) {globals.newSymbol($ID.text);} -> dek(n={$ID.text});
  catch [RuntimeException ex] {errorID(ex,$i1);} 

expr        : ^(PLUS  e1=expr e2=expr)                       ->    add(p1={$e1.st},  p2={$e2.st})
            | ^(MINUS e1=expr e2=expr)                       ->    sub(p1={$e1.st},  p2={$e2.st})
            | ^(MUL   e1=expr e2=expr)                       ->    mul(p1={$e1.st},  p2={$e2.st})
            | ^(DIV   e1=expr e2=expr)                       ->    div(p1={$e1.st},  p2={$e2.st})
            | ^(EQ    e1=expr e2=expr) {condition_number++;} ->     eq(p1={$e1.st},  p2={$e2.st}, nr={condition_number.toString()})
            | ^(NEQ   e1=expr e2=expr) {condition_number++;} ->    neq(p1={$e1.st},  p2={$e2.st}, nr={condition_number.toString()})
            | ^(GT    e1=expr e2=expr) {condition_number++;} ->     gt(p1={$e1.st},  p2={$e2.st}, nr={condition_number.toString()})
            | ^(LT    e1=expr e2=expr) {condition_number++;} ->     lt(p1={$e1.st},  p2={$e2.st}, nr={condition_number.toString()})
            | ^(PODST i1=ID   e2=expr)                       -> setVar(id={$ID.text}, e={$e2.st})
            |   ID                                           -> getVar(id={$ID.text})
            |   INT                               {numer++;} ->    int( i={$INT.text}, j={ numer.toString() })
            | ^(IF    e1=expr e2=expr e3=expr?)   {numer++;} -> ifthen(e1={$e1.st}, e2={$e2.st}, e3={$e3.st}, nr={ numer.toString() })
            | ^(WHILE e1=expr e2=expr)            {numer++;} ->  while(e1={$e1.st}, e2={$e2.st}, nr={ numer.toString() })
            ;
