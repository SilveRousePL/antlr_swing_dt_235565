package tb.antlr.interpreter;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;
import tb.antlr.symbolTable.GlobalSymbols;

public class MyTreeParser extends TreeParser {
	
	private GlobalSymbols globalSymbols = new GlobalSymbols();

    public MyTreeParser(TreeNodeStream input) {
        super(input);
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }

    protected void drukuj(String text) {
        System.out.println(text.replace('\r', ' ').replace('\n', ' '));
    }

	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}
	
	protected Integer add(Integer a, Integer b) {
		return a+b;
	}
	
	protected Integer sub(Integer a, Integer b) {
		return a-b;
	}
	
	protected Integer mul(Integer a, Integer b) {
		return a*b;
	}
	
	protected Integer div(Integer a, Integer b) {
		return a/b;
	}
	
	protected void declareVar(String id) {
		globalSymbols.newSymbol(id);
	}
	
	protected void setVar(String id, Integer val) {
		globalSymbols.setSymbol(id, val);
	}
	
	protected Integer getVar(String id) {
		return globalSymbols.getSymbol(id);
	}
}
