grammar Expr;

options {
  output=AST;
  ASTLabelType=CommonTree;
}

@header {
package tb.antlr;
}

@lexer::header {
package tb.antlr;
}

prog
    : (stat | scopeBlock)+ EOF!
    ;

scopeBlock
    : LC^ (stat | scopeBlock)* RC!
    ;

stat
    : expr NL -> expr
    | VAR ID (PODST expr)? NL -> ^(VAR ID) ^(PODST ID expr)?
    | ID PODST expr NL -> ^(PODST ID expr)
    | if_stat NL -> if_stat
    | loop NL -> loop
    | NL ->
    ;

if_stat
	  : IF^ LP! expr RP! THEN! expr (ELSE! expr)?
	  ;
	  
loop
    : WHILE^ LP! expr RP! expr
    ;

expr
    : addExpr
      ( EQ^ addExpr
      | NEQ^ addExpr
      | GT^ addExpr
      | LT^ addExpr
      )*
    ;

addExpr
    : multExpr
      ( PLUS^ multExpr
      | MINUS^ multExpr
      )*
    ;

multExpr
    : atom
      ( MUL^ atom
      | DIV^ atom
      | MOD^ atom
      )*
    ;

atom
    : INT
    | ID
    | LP! expr RP!
    ;

VAR :'var';

LC
  : '{'
  ;

RC
  : '}'
  ;

IF
  : 'if'
  ;
  
THEN
  : 'then'
  ;

ELSE
  : 'else'
  ;
  
WHILE
  : 'while'
  ;

ID : ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*;

INT : '0'..'9'+;

NL : '\r'? '\n' ;

WS : (' ' | '\t')+ {$channel = HIDDEN;} ;

LP
	:	'('
	;

RP
	:	')'
	;

PODST
	:	'='
	;

PLUS
	:	'+'
	;

MINUS
	:	'-'
	;

MUL
	:	'*'
	;

DIV
	:	'/'
	;
	
MOD
  : '%'
  ;
	
EQ
  : '=='
  ;
  
NEQ
  : '!='
  ;
  
GT
  : '>'
  ;
  
LT
  : '<'
  ;
